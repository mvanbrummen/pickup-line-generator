package com.example.michael.pickuplinegenerator;


import android.app.Application;

import java.util.Random;

public class App extends Application {

    private static App instance = null;

    private Random rand = null;
    private int currentIdx = 0;

    private String[] LINES;

    public static App getInstance() {
        checkInstance();
        return instance;
    }

    private static void checkInstance() {
        if (instance == null) {
            throw new IllegalStateException();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        LINES = getResources().getStringArray(R.array.lines);
        rand = new Random();
    }

    public String getPickupLine() {
        int newIdx;
        do {
            newIdx = randInt();
        } while(newIdx == currentIdx);
        currentIdx = newIdx;
        return LINES[newIdx];
    }

    private int randInt() {
        return randInt(0, LINES.length - 1);
    }

    private int randInt(int min, int max) {
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

}

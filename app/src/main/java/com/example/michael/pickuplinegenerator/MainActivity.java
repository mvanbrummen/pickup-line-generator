package com.example.michael.pickuplinegenerator;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView pickupText;
    private ImageView generateBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUIComponents();
    }

    private void setUIComponents() {
        Window window = this.getWindow();
        window.setStatusBarColor(Color.parseColor("#FF1E1E1E"));
        pickupText = (TextView) findViewById(R.id.pickup_text);
        pickupText.setText(App.getInstance().getPickupLine());
        generateBtn = (ImageView) findViewById(R.id.generate_button);
        generateBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        generateBtn.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.shake));
        pickupText.setText(App.getInstance().getPickupLine());
    }

}
